<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Ejercicio 5</title>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
		<link href="https://fonts.googleapis.com/css?family=Roboto|Roboto+Mono" rel="stylesheet">
    </head>
    <body>
        <div class="container text-center mb-3">
			<h1 class="h1">Ejercicio número 5</h1>
		</div>
		<div class="container">
        	<div class="row">
        		<div class="col-12 text-center">
        			<p>Hallar el factorial de un número N. El factorial de un número se define para números enteros positivos como: N!= N*(N-1)*(N-2)*... *3*2*1, por ejemplo 5! = 5 * 4 * 3 * 2 * 1 = 120. Por definición 0!= 1 y 1!=1.</p>
        		</div>
        	</div>
        </div>
        <section>
        	<form action="" method="post">
	        	<div class="container">
	        		<div class="row">
	        			<div class="col-12 col-md-6 text-center">
	        				<label for="nFactorial">Ingrese el número al cual se le va a hallar el factorial</label>
	        			</div>
	        			<div class="col-12 col-md-6">
	        				<input type="number" id="nFactorial" name="nFactorial">
	        			</div>
	        		</div>
	        		<div class="row">
	        			<div class="col-12 col-md-6 text-center">
	        				<p>El <?php echo numero(); ?>! es : <?php echo suma(); ?></p>
	        			</div>
	        			<div class="col-12 col-md-6" id="factorial">
	        				<?php
	        					if (!empty($_POST['nFactorial'])) {
								$numFact = $_POST['nFactorial'];
								$acum = 1;
								$txt = $numFact.'! = ';
								for ($i= $numFact; $i >= 1 ; $i--) 
									{ if ($i == 1) {
										$acum = $acum * 1;
										$txt = $txt.''.$i;
									 }else{
									 	$acum = $acum * $i;
									 	$txt = $txt.''.$i.' * ';
									 }
								}
								$txt = $txt.' = '.$acum;
								echo $txt;
								}else{
									echo "";
								}
	        				?>
	        			</div>
	        		</div>
	        		<div class="row">
						<div class="col-12 text-center">
							<input type="submit" value="Hallar factorial" class="btn btn-dark">
						</div>
					</div>
	        	</div>
        	</form>
        </section>
		<?php 
			function numero()
			{
				if (!empty($_POST['nFactorial'])) {
					$numFact = $_POST['nFactorial'];
				}else{
					$numFact = "";
				}
				return $numFact;
			}

			function suma()
			{
				if (!empty($_POST['nFactorial'])) {
					$numFact = $_POST['nFactorial'];
					$acum = 1;
					for ($i= $numFact; $i >= 1 ; $i--) { 
						$acum = $acum * $i;
					}
				}else{
					$acum = "";
				}
				return $acum;
			}
		?>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    </body>
</html>