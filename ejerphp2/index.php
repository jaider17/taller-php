<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Untitled</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
		<link href="https://fonts.googleapis.com/css?family=Roboto|Roboto+Mono" rel="stylesheet">
    </head>
    <body>
    	
        <div class="container text-center mb-3">
			<h1 class="h1">Ejercicio número 2</h1>
		</div>
		<div class="container">
			<div class="row">
				<div class="col text-center">
					<p>Calcule la suma de los primeros M términos de la serie 1 + 1/2 + 1/3 + /4 +... + 1/M (esta es la serie Aritmética).</p>
				</div>
			</div>
		</div>

		<section>
			<form action="" method="post" id="frmDatos" name="frmDatos">
				<div class="row">
						<div class="col-12 col-md-6 text-center">
							<label for="nmrFin"> Igrese el número final de la serie. </label>
						</div>
						<div class="col-12 col-md-6">
							<input type="number" id="nmrFin" name="nmrFin" placeholder=" no puede ser 0">
						</div>
				</div>
					<?php 

					function operacion()
					{
						$txt = "";
						if (!empty($_POST['nmrFin'])) {
							$fin = $_POST['nmrFin'];
							$suma = 0;
							for ($i=1; $i <= $fin ; $i++) { 
								$suma += 1/$i;
								if ($i == $fin) {
									$txt = $txt.'(1 / '.$i.' )';
								}else{
									$txt = $txt.'(1 / '.$i.' ) + ';
								}
							}
							$text = '
									<div class="row">
										<div class="col-12 col-md-6 text-center">
											<label for="sumatoria">El total de la sumatoria es de :</label>
										</div>
										<div class="col-12 col-md-6" id="sumatoria">
											'.$txt.'	
										</div>
									</div>
									<div class="row">
										<div class="col-12 col-md-6 text-center">
											<label>La sumatoria es :</label>
										</div>
										<div class="col-12 col-md-6" id="textoSuma">
											'.$suma.'
										</div>
									</div>';
						}else{
							$text = '<div class="row my-3">
										<div class="col-12 text-center">
											<p>Ingrese un número que sea diferente de 0</p>
										</div>
									</div>';
						}
						return $text;
					}
	    		?>
	    		<div >
	    			<?php echo operacion(); ?>
	    		</div>
				<div class="row my-3">
					<div class="col-12 text-center">
						<input type="submit" class="btn btn-dark" value="Mostrar resultado">
					</div>
				</div>
			</form>
		</section>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    </body>
</html>